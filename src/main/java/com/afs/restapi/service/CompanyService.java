package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private final JPACompanyRepository jpaCompanyRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
    }


    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        Page<Company> companyPage = jpaCompanyRepository.findAll(pageRequest);
        return companyPage.getContent();
    }

    public CompanyResponse findById(Long id) {
        Company company = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        return CompanyMapper.toResponse(company);
    }

    public void update(Long id, CompanyRequest companyRequest) {
        Company preCompany = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        if (companyRequest.getName() != null) {
            preCompany.setName(companyRequest.getName());
        }
        jpaCompanyRepository.save(preCompany);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = CompanyMapper.toEntity(companyRequest);
        Company savedCompany = jpaCompanyRepository.save(company);
        return CompanyMapper.toResponse(savedCompany);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
//        return getEmployeeRepository().findByCompanyId(id);
        return jpaCompanyRepository.findById(id).map(Company::getEmployees).orElseThrow(CompanyNotFoundException::new);
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
